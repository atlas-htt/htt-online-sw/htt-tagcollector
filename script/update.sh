#!/bin/bash
cp ../tagcollector.csv . 
i=0
while read line
  do
    echo "Reading line:" $line
    if [ $i == 0 ]; then
      set $line
      tag=$1
      echo "Release version: $tag"
      i=$i+1
      continue
    fi
    #ai=$i+1
      set $line
      echo "Updating $1 to $2"
( cd ../../htt-release/$1 && \
      git fetch && \
      git fetch --tags && \
      git checkout $2 )
  done < tagcollector.csv
cd ../../htt-release/
git add .
git commit -m 'Update the submodule to the "my-tag" version' -a
git tag $tag
